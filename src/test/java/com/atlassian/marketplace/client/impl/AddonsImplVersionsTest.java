package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonVersionsQuery;
import com.atlassian.marketplace.client.model.AddonVersion;
import com.atlassian.marketplace.client.model.AddonVersionSummary;
import com.atlassian.marketplace.client.model.TestModelBuilders;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.api.AddonVersionSpecifier.buildNumber;
import static com.atlassian.marketplace.client.api.AddonVersionSpecifier.latest;
import static com.atlassian.marketplace.client.api.AddonVersionSpecifier.versionName;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_ADDONS_PATH;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonVersion;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;

public class AddonsImplVersionsTest extends AddonsImplTestBase<AddonVersion>
{
    @Test
    public void getVersionByBuildUsesAppropriateUri() throws Exception
    {
        setupVersionResource("x", versionByBuildUri(1000000000001L));
        assertThat(tester.client.addons().safeGetVersion("x", buildNumber(1000000000001L), AddonVersionsQuery.any()),
                   equalTo(Optional.of(ADDON_VERSION_REP)));
    }

    @Test
    public void getVersionByNameUsesAppropriateUri() throws Exception
    {
        setupVersionResource("x", versionByNameUri("1.0"));
        assertThat(tester.client.addons().safeGetVersion("x", versionName("1.0"), AddonVersionsQuery.any()),
                   equalTo(Optional.of(ADDON_VERSION_REP)));
    }
    
    @Test
    public void getLatestVersionUsesAddonResource() throws Exception
    {
        setupVersionResource("x", latestVersionUri("x"));
        tester.client.addons().safeGetVersion("x", latest(), AddonVersionsQuery.any());
        verify(tester.httpTransport).get(URI.create(HOST_BASE + FAKE_ADDONS_PATH + "?limit=0"));
    }

    @Test
    public void getLatestVersionUsesLatestVersionResource() throws Exception
    {
        setupVersionResource("x", latestVersionUri("x"));
        assertThat(tester.client.addons().safeGetVersion("x", latest(), AddonVersionsQuery.any()), equalTo(Optional.of(ADDON_VERSION_REP)));
    }

    @Test
    public void getLatestVersionPassesOtherParamsInQueryString() throws Exception
    {
        // Since we use the same code for encoding all AddonQuery parameters in general, we'll just verify that this
        // resource can pass any of those parameters.
        setupVersionResource("x", latestVersionUri("x").queryParam("application", "jira"));
        assertThat(tester.client.addons().safeGetVersion("x", latest(), AddonVersionsQuery.builder().application(Optional.of(JIRA)).build()),
                   equalTo(Optional.of(ADDON_VERSION_REP)));
    }

    @Test
    public void getLatestVersionAfterPassesAfterVersionInQueryString() throws Exception
    {
        setupVersionResource("x", latestVersionUri("x").queryParam("afterVersion", "1.0"));
        assertThat(tester.client.addons().safeGetVersion("x", latest(), AddonVersionsQuery.builder().afterVersion(Optional.of("1.0")).build()),
                   equalTo(Optional.of(ADDON_VERSION_REP)));
    }

    @Test
    public void getLatestVersionAfterPassesOtherParamsInQueryString() throws Exception
    {
        // Since we use the same code for encoding all AddonQuery parameters in general, we'll just verify that this
        // resource can pass any of those parameters.
        setupVersionResource("x", latestVersionUri("x").queryParam("application", "jira").queryParam("afterVersion", "1.0"));
        assertThat(tester.client.addons().safeGetVersion("x", latest(),
            AddonVersionsQuery.builder().application(Optional.of(JIRA)).afterVersion(Optional.of("1.0")).build()),
                   equalTo(Optional.of(ADDON_VERSION_REP)));
    }
    
    @Test
    public void getVersionsPassesQueryParams() throws Exception
    {
        AddonVersionSummary version1 = TestModelBuilders.addonVersionSummary().name("1.0").build();
        AddonVersionSummary version2 = TestModelBuilders.addonVersionSummary().name("2.0").build();
        InternalModel.AddonVersions versionsRep =
            InternalModel.addonVersions(ADDON_VERSIONS_LINKS, ImmutableList.of(version1, version2), 2);
        
        setupAddonByKeyResource(addonByKeyUri("x"));
        tester.mockResource(UriBuilder.fromUri(HOST_BASE + FAKE_ADDON_VERSIONS_PATH)
            .queryParam("application", "jira").build(), versionsRep);
        assertThat(tester.client.addons().getVersions("x",
            AddonVersionsQuery.builder().application(Optional.of(JIRA)).build()),
            contains(version1, version2));
    }

    @Test
    public void getVersionsPassesAccessToken() throws Exception
    {
        setupAddonByKeyResource(addonByKeyUri("x").queryParam("accessToken", "xyz"));
        tester.mockResource(
            UriBuilder.fromUri(HOST_BASE + FAKE_ADDON_VERSIONS_PATH).queryParam("accessToken", "xyz").queryParam("limit", 0).build(),
            ADDON_VERSIONS_REP);
        tester.mockResource(
            latestVersionUri("x").queryParam("accessToken", "xyz").build(),
            ADDON_VERSION_REP);
        assertThat(tester.client.addons().safeGetVersion("x", latest(),
                AddonVersionsQuery.builder().accessToken(Optional.of("xyz")).build()),
            equalTo(Optional.of(ADDON_VERSION_REP)));
    }
    
    @Override
    protected URI getCollectionUri()
    {
        return URI.create(HOST_BASE + FAKE_ADDON_VERSIONS_PATH);
    }
    
    @Override
    protected AddonVersion makeEntityToCreate()
    {
        return addonVersion().build();
    }
    
    @Override
    protected AddonVersion makeEntityFromServer()
    {
        return addonVersion().links(ADDON_LINKS).build();
    }
    
    @Override
    protected AddonVersion makeEntityWithUpdates(AddonVersion from)
    {
        return addonVersion(from).build();
    }
    
    @Override
    protected AddonVersion createEntity(AddonVersion entity) throws Exception
    {
        setupAddonByKeyResource(addonByKeyUri("x"));
        return tester.client.addons().createVersion("x", entity);
    }
    
    @Override
    protected AddonVersion updateEntity(AddonVersion original, AddonVersion updated) throws MpacException
    {
        return tester.client.addons().updateVersion(original, updated);
    }
}
