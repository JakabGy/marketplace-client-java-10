/**
 * Interfaces and immutable value classes used in the Atlassian Marketplace API.
 * <p>
 * This package includes interfaces for groups of related API methods that are accessed through
 * {@link com.atlassian.marketplace.client.MarketplaceClient} ({@link com.atlassian.marketplace.client.api.Addons}, etc.); classes
 * representing complex parameters for API methods ({@link com.atlassian.marketplace.client.api.AddonQuery}, etc.); and interfaces
 * for opaque objects returned by API methods ({@link com.atlassian.marketplace.client.api.Page}).
 * <p>
 * Classes that correspond to JSON objects returned by the server's REST API are in
 * the {@link com.atlassian.marketplace.client.model} package.
 */
package com.atlassian.marketplace.client.api;