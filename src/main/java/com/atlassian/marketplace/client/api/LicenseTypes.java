package com.atlassian.marketplace.client.api;

import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.LicenseType;

import java.util.Optional;

/**
 * Starting point for all resources that return software license type information.  Use
 * {@link MarketplaceClient#licenseTypes()} to access this API.
 * @since 2.0.0
 */
public interface LicenseTypes
{
    /**
     * Returns all of the available license types.
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Iterable<LicenseType> getAllLicenseTypes() throws MpacException;
    
    /**
     * Searches for a license type by its "key" property - for instance, "gpl" or "commercial".
     * The resource URIs for license types may change in the future, but their keys will not change.
     * @param licenseKey  the key of the desired license type
     * @return  the license type object, or {@link Optional#empty()} if no such license type exists
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Optional<LicenseType> safeGetByKey(String licenseKey) throws MpacException;

}
