package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReader;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.model.Links;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.marketplace.client.util.Convert.toOptional;
import static com.google.common.base.Preconditions.checkNotNull;

final class PageImpl<T> extends Page<T>
{
    public static final String NEXT_REL = "next";
    public static final String PREVIOUS_REL = "prev";
    
    private final PageReference<T> reference;
    private final Optional<URI> previousUri;
    private final Optional<URI> nextUri;
    
    PageImpl(PageReference<T> reference, Links links, Iterable<T> items, int count, PageReader<T> reader)
    {
        super(items, count, reader);
        this.reference = checkNotNull(reference, "reference");
        checkNotNull(links, "links");
        this.previousUri = toOptional(links.getUri(PREVIOUS_REL));
        this.nextUri = toOptional(links.getUri(NEXT_REL));
    }

    @Override
    public Optional<PageReference<T>> safeGetReference()
    {
        return Optional.of(reference);
    }

    @Override
    public Optional<PageReference<T>> safeGetPrevious()
    {
        return previousUri.map(
                p -> new PageReference<T>(
                        p,
                        QueryBounds
                                .offset(getBounds().getOffset() - getBounds().safeGetLimit().orElseGet(this::size))
                                .withLimit(Optional.of(getBounds().safeGetLimit().orElseGet(this::size))),
                        reader
                )
        );
    }

    @Override
    public Optional<PageReference<T>> safeGetNext()
    {
        return nextUri.map(
                n -> new PageReference<T>(
                        n,
                        QueryBounds
                                .offset(getBounds().getOffset() + getBounds().safeGetLimit().orElseGet(this::size))
                                .withLimit(Optional.of(getBounds().safeGetLimit().orElseGet(this::size))),
                        reader
                )
        );
    }

    private QueryBounds getBounds()
    {
        return reference.getBounds();
    }
}
